<?php require_once '../header.php'?>
        <div class="container">
            <div class="line mt-4">
                <span class="line-text">
                    Каталог
                </span>
            </div>
            <div class="row mt-5 mb-2">
                <div class="col-md-3 mt-3 mb-3">
                    <div class="card hovered-mask">
                        <img src="/weblab/images/catalog/goods/1.png" class="img-fluid card-img-top" alt="...">
                        <div class="card-body">
                            <div class="card-img-overlay card-info">
                                <p>
                                    <h3 class="card-title text-light text-center">Просмотр изделия</h3>
                                </p>
                                <p class="card-text text-center">
                                    <button type="button" class="btn btn-outline-light">Просмотр</button>
                                </p>
                            </div>
                            <h4 class="card-title text-center">3 405 р.</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mt-3 mb-3">
                    <div class="card hovered-mask">
                        <img src="/weblab/images/catalog/goods/2.png" class="img-fluid card-img-top" alt="...">
                        <div class="card-body">
                            <div class="card-img-overlay card-info">
                                <p>
                                    <h3 class="card-title text-light text-center">Просмотр изделия</h3>
                                </p>
                                <p class="card-text text-center">
                                    <button type="button" class="btn btn-outline-light">Просмотр</button>
                                </p>
                            </div>
                            <h4 class="card-title text-center">5 533 р.</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mt-3 mb-3">
                    <div class="card hovered-mask">
                        <img src="/weblab/images/catalog/goods/3.png" class="img-fluid card-img-top" alt="...">
                        <div class="card-body">
                            <div class="card-img-overlay card-info">
                                <p>
                                    <h3 class="card-title text-light text-center">Просмотр изделия</h3>
                                </p>
                                <p class="card-text text-center">
                                    <button type="button" class="btn btn-outline-light">Просмотр</button>
                                </p>
                            </div>
                            <h4 class="card-title text-center">4 100 р.</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mt-3 mb-3">
                    <div class="card hovered-mask">
                        <img src="/weblab/images/catalog/goods/4.png" class="img-fluid card-img-top" alt="...">
                        <div class="card-body">
                            <div class="card-img-overlay card-info">
                                <p>
                                    <h3 class="card-title text-light text-center">Просмотр изделия</h3>
                                </p>
                                <p class="card-text text-center">
                                    <button type="button" class="btn btn-outline-light">Просмотр</button>
                                </p>
                            </div>
                            <h4 class="card-title text-center">3 798 р.</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mt-3 mb-3">
                    <div class="card hovered-mask">
                        <img src="/weblab/images/catalog/goods/5.png" class="img-fluid card-img-top" alt="...">
                        <div class="card-body">
                            <div class="card-img-overlay card-info">
                                <p>
                                    <h3 class="card-title text-light text-center">Просмотр изделия</h3>
                                </p>
                                <p class="card-text text-center">
                                    <button type="button" class="btn btn-outline-light">Просмотр</button>
                                </p>
                            </div>
                            <h4 class="card-title text-center">4 556 р.</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mt-3 mb-3">
                    <div class="card hovered-mask">
                        <img src="/weblab/images/catalog/goods/6.png" class="img-fluid card-img-top" alt="...">
                        <div class="card-body">
                            <div class="card-img-overlay card-info">
                                <p>
                                    <h3 class="card-title text-light text-center">Просмотр изделия</h3>
                                </p>
                                <p class="card-text text-center">
                                    <button type="button" class="btn btn-outline-light">Просмотр</button>
                                </p>
                            </div>
                            <h4 class="card-title text-center">9 542 р.</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mt-3 mb-3">
                    <div class="card hovered-mask">
                        <img src="/weblab/images/catalog/goods/7.png" class="img-fluid card-img-top" alt="...">
                        <div class="card-body">
                            <div class="card-img-overlay card-info">
                                <p>
                                    <h3 class="card-title text-light text-center">Просмотр изделия</h3>
                                </p>
                                <p class="card-text text-center">
                                    <button type="button" class="btn btn-outline-light">Просмотр</button>
                                </p>
                            </div>
                            <h4 class="card-title text-center">21 598 р.</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mt-3 mb-3">
                    <div class="card hovered-mask">
                        <img src="/weblab/images/catalog/goods/8.png" class="img-fluid card-img-top" alt="...">
                        <div class="card-body">
                            <div class="card-img-overlay card-info">
                                <p>
                                    <h3 class="card-title text-light text-center">Просмотр изделия</h3>
                                </p>
                                <p class="card-text text-center">
                                    <button type="button" class="btn btn-outline-light">Просмотр</button>
                                </p>
                            </div>
                            <h4 class="card-title text-center">15 167 р.</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mt-3 mb-3">
                    <div class="card hovered-mask">
                        <img src="/weblab/images/catalog/goods/9.png" class="img-fluid card-img-top" alt="...">
                        <div class="card-body">
                            <div class="card-img-overlay card-info">
                                <p>
                                    <h3 class="card-title text-light text-center">Просмотр изделия</h3>
                                </p>
                                <p class="card-text text-center">
                                    <button type="button" class="btn btn-outline-light">Просмотр</button>
                                </p>
                            </div>
                            <h4 class="card-title text-center">2 210 р.</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mt-3 mb-3">
                    <div class="card hovered-mask">
                        <img src="/weblab/images/catalog/goods/10.png" class="img-fluid card-img-top" alt="...">
                        <div class="card-body">
                            <div class="card-img-overlay card-info">
                                <p>
                                    <h3 class="card-title text-light text-center">Просмотр изделия</h3>
                                </p>
                                <p class="card-text text-center">
                                    <button type="button" class="btn btn-outline-light">Просмотр</button>
                                </p>
                            </div>
                            <h4 class="card-title text-center">5 275 р.</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mt-3 mb-3">
                    <div class="card hovered-mask">
                        <img src="/weblab/images/catalog/goods/11.png" class="img-fluid card-img-top" alt="...">
                        <div class="card-body">
                            <div class="card-img-overlay card-info">
                                <p>
                                    <h3 class="card-title text-light text-center">Просмотр изделия</h3>
                                </p>
                                <p class="card-text text-center">
                                    <button type="button" class="btn btn-outline-light">Просмотр</button>
                                </p>
                            </div>
                            <h4 class="card-title text-center">15 421 р.</h4>
                        </div>
                    </div>
                </div>             
                <div class="col-md-3 mt-3 mb-3">
                    <div class="card hovered-mask">
                        <img src="/weblab/images/catalog/goods/12.png" class="img-fluid card-img-top" alt="...">
                        <div class="card-body">
                            <div class="card-img-overlay card-info">
                                <p>
                                    <h3 class="card-title text-light text-center">Просмотр изделия</h3>
                                </p>
                                <p class="card-text text-center">
                                    <button type="button" class="btn btn-outline-light">Просмотр</button>
                                </p>
                            </div>
                            <h4 class="card-title text-center">7 798 р.</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php require_once '../footer.php'?>