<footer class="page-footer font-small blue-grey lighten-5">
            <hr>
            <div class="container">
                <div class="row py-1 d-flex align-items-center">
                    <div class="col-md-8 text-md-left mb-4 mb-md-0">
                        <nav class="nav lighten-4">
                            <a class="nav-link" href="#">
                                <h6 class="text-uppercase text-body text-center font-weight-bold">О бренде</h6>
                            </a>
                            <a class="nav-link" href="#">
                                <h6 class="text-uppercase text-body text-center font-weight-bold">Украшения</h6>
                            </a>
                            <a class="nav-link" href="#">
                                <h6 class="text-uppercase text-body text-center font-weight-bold">Часы</h6>
                            </a>
                            <a class="nav-link" href="#">
                                <h6 class="text-uppercase text-body text-center font-weight-bold">Информация</h6>
                            </a>
                            <a class="nav-link" href="#">
                                <h6 class="text-uppercase text-body text-center font-weight-bold">Партнерам</h6>
                            </a>
                        </nav>
                    </div>
                    <div class="col-md-4 col-lg-4 text-center text-md-right">
                        <div class="row">
                            <div class="col-md-6">
                                <a>
                                    <img class="img-fluid" src="/weblab/images/catalog/gplay.png"> 
                                </a>
                            </div>
                            <div class="col-md-6">
                                <a>
                                    <img class="img-fluid" src="/weblab/images/catalog/appstore.png">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="container">
                <div class="row py-1 d-flex align-items-center">
                    <div class="col-md-8 text-md-left mb-4 mb-md-0">
                        ©2020 - Form | All right reserved
                    </div>
                    <div class="col-md-4 text-center text-md-right">
                        <div class="d-flex flex-row-reverse">
                            <div class="p-3">
                                <svg id="Capa_1" enable-background="new 0 0 512 512" height="20" viewBox="0 0 512 512" width="20" xmlns="http://www.w3.org/2000/svg">
                                    <g>
                                        <path d="m405.102 0h-298.204c-58.944 0-106.898 47.954-106.898 106.898v298.203c0 58.945 47.954 106.899 106.898 
                                            106.899h298.203c58.945 0 106.899-47.954 106.899-106.898v-298.204c0-58.944-47.954-106.898-106.898-106.898zm-298.204 
                                            482c-42.402 0-76.898-34.496-76.898-76.898v-200.761h79.52c-8.362 19.351-13.006 40.669-13.006 63.055 0 87.941 71.545 
                                            159.486 159.486 159.486s159.486-71.545 159.486-159.486c0-22.385-4.645-43.704-13.006-63.055h79.52v200.761c0 42.402-34.496 
                                            76.898-76.898 76.898zm298.204-452c42.402 0 76.898 34.496 76.898 76.898v67.442h-96.555c-28.984-40.206-76.207-66.432-129.445-66.432s-100.461 
                                            26.226-129.445 66.432h-96.555v-67.442c0-42.402 34.496-76.898 76.898-76.898zm-19.616 237.396c0 71.399-58.087 129.486-129.486 
                                            129.486s-129.486-58.087-129.486-129.486 58.087-129.487 129.486-129.487 129.486 58.087 129.486 129.487z"/>
                                        <path d="m397.296 85h30v30h-30z"/>
                                        <path d="m164.882 267.396c0 50.242 40.875 91.117 91.118 91.117s91.118-40.875 91.118-91.117c0-50.243-40.875-91.118-91.118-91.118s-91.118 
                                            40.874-91.118 91.118zm152.236 0c0 33.7-27.418 61.117-61.118 61.117s-61.118-27.417-61.118-61.117 27.418-61.119 61.118-61.119 61.118 27.418 
                                            61.118 61.119z"/>
                                    </g>
                                </svg>
                            </div>
                            <div class="p-3">
                                <svg id="Bold" enable-background="new 0 0 24 24" height="20" viewBox="0 0 24 24" width="20" xmlns="http://www.w3.org/2000/svg">
                                    <path d="m15.997 3.985h2.191v-3.816c-.378-.052-1.678-.169-3.192-.169-3.159 0-5.323 1.987-5.323 
                                        5.639v3.361h-3.486v4.266h3.486v10.734h4.274v-10.733h3.345l.531-4.266h-3.877v-2.939c.001-1.233.333-2.077 
                                        2.051-2.077z"/>
                                </svg>
                            </div>
                            <div class="p-3">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="20" height="20"
                                    viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <path d="M512,97.248c-19.04,8.352-39.328,13.888-60.48,16.576c21.76-12.992,38.368-33.408,46.176-58.016
                                            c-20.288,12.096-42.688,20.64-66.56,25.408C411.872,60.704,384.416,48,354.464,48c-58.112,0-104.896,47.168-104.896,104.992
                                            c0,8.32,0.704,16.32,2.432,23.936c-87.264-4.256-164.48-46.08-216.352-109.792c-9.056,15.712-14.368,33.696-14.368,53.056
                                            c0,36.352,18.72,68.576,46.624,87.232c-16.864-0.32-33.408-5.216-47.424-12.928c0,0.32,0,0.736,0,1.152
                                            c0,51.008,36.384,93.376,84.096,103.136c-8.544,2.336-17.856,3.456-27.52,3.456c-6.72,0-13.504-0.384-19.872-1.792
                                            c13.6,41.568,52.192,72.128,98.08,73.12c-35.712,27.936-81.056,44.768-130.144,44.768c-8.608,0-16.864-0.384-25.12-1.44
                                            C46.496,446.88,101.6,464,161.024,464c193.152,0,298.752-160,298.752-298.688c0-4.64-0.16-9.12-0.384-13.568
                                            C480.224,136.96,497.728,118.496,512,97.248z"/>
                                    </g>
                                </svg>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </footer>

        <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
        <script src="/weblab/js/custom.js"></script>
    </body>
</html>