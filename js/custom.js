var mySwiper = new Swiper('.swiper-container', {
    speed: 400,
    spaceBetween: 200,
    centeredSlides: true,
    centeredSlidesBounds: true,
    autoHeight: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: '.swiper-pagination',
    }
});