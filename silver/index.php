<?php require_once '../header.php'?>
        <div class="container">
            <div class="row py-1 d-flex align-items-center">
                <div class="col-md-8 text-md-left mb-4 mb-md-0">
                    <span class="text-dark">
                        <h1>
                            Все цены снижены!<br>
                            +10% ДОПОЛНИТЕЛЬНО
                        </h1>
                    </span>
                </div>
                <div class="col-md-4 text-center text-md-right">
                    <span class="text-muted">Смотреть новинки</span>
                </div>
            </div>
            <div class="row mt-5 mb-2">
                <div class="col-md-6 mt-3 mb-3">
                    <div class="card img-mask-1">
                        <img src="/weblab/images/catalog/silver/1.png" class="img-fluid card-img-top" alt="...">
                        <div class="card-body">
                            <div class="card-top-right">
                                <div class="card-percent">
                                    -30%
                                </div>
                                <div class="my-4 bd-highlight">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 28 28" data-code="61578" data-tags="heart-o">
                                        <g fill="#444" transform="scale(0.02734375 0.02734375)">
                                            <path d="M950.857 340.571c0-160.571-108.571-194.286-200-194.286-85.143 0-181.143 92-210.857 
                                                127.429-13.714 16.571-42.286 16.571-56 0-29.714-35.429-125.714-127.429-210.857-127.429-91.429 
                                                0-200 33.714-200 194.286 0 104.571 105.714 201.714 106.857 202.857l332 320 331.429-319.429c1.714-1.714 
                                                107.429-98.857 107.429-203.429zM1024 340.571c0 137.143-125.714 252-130.857 257.143l-356 342.857c-6.857 
                                                6.857-16 10.286-25.143 10.286s-18.286-3.429-25.143-10.286l-356.571-344c-4.571-4-130.286-118.857-130.286-256 
                                                0-167.429 102.286-267.429 273.143-267.429 100 0 193.714 78.857 238.857 123.429 45.143-44.571 138.857-123.429 
                                                238.857-123.429 170.857 0 273.143 100 273.143 267.429z" />
                                        </g>
                                    </svg>
                                </div>
                                <div class="my-4 bd-highlight">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20" data-tags="share-alt">
                                        <g fill="#444" transform="scale(0.01953125 0.01953125)">
                                            <path d="M260.096 622.592c-27.575 26.972-65.35 43.615-107.012 43.615-84.548 0-153.088-68.54-153.088-153.088 
                                            0-0.393 0.001-0.787 0.004-1.179l-0 0.060c0.054-84.79 68.802-153.505 153.6-153.505 
                                            41.373 0 78.926 16.358 106.544 42.959l-0.048-0.046 457.728-228.864c-0.768-5.77-1.206-12.441-1.206-19.214 0-84.831 
                                            68.769-153.6 153.6-153.6s153.6 68.769 153.6 153.6c0 84.831-68.769 153.6-153.6 153.6-41.285 
                                            0-78.765-16.288-106.366-42.788l0.052 0.050-457.728 228.864c0.739 5.688 1.161 12.267 1.161 
                                            18.944s-0.422 13.256-1.24 19.711l0.079-0.767 457.728 228.864c27.575-26.972 65.35-43.615 107.012-43.615 
                                            84.548 0 153.088 68.54 153.088 153.088 0 0.393-0.001 0.787-0.004 1.179l0-0.060c-0.727 84.277-69.215 
                                            152.316-153.595 152.316-84.831 0-153.6-68.769-153.6-153.6 0-6.223 0.37-12.36 1.089-18.389l-0.071 0.729-457.728-228.864z" />
                                        </g>
                                    </svg>
                                </div>
                            </div>
                            <p>
                                <h6 class="card-title">Кольцо</h6>
                            </p>
                            <p>
                                3 405 р.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mt-3 mb-3">
                    <div class="card img-mask-1">
                        <img src="/weblab/images/catalog/silver/2.png" class="img-fluid card-img-top" alt="...">
                        <div class="card-body">
                            <div class="card-top-right">
                                <div class="card-percent">
                                    -10%
                                </div>
                                <div class="my-4 bd-highlight">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 28 28" data-code="61578" data-tags="heart-o">
                                        <g fill="#444" transform="scale(0.02734375 0.02734375)">
                                            <path d="M950.857 340.571c0-160.571-108.571-194.286-200-194.286-85.143 0-181.143 92-210.857 
                                                127.429-13.714 16.571-42.286 16.571-56 0-29.714-35.429-125.714-127.429-210.857-127.429-91.429 
                                                0-200 33.714-200 194.286 0 104.571 105.714 201.714 106.857 202.857l332 320 331.429-319.429c1.714-1.714 
                                                107.429-98.857 107.429-203.429zM1024 340.571c0 137.143-125.714 252-130.857 257.143l-356 342.857c-6.857 
                                                6.857-16 10.286-25.143 10.286s-18.286-3.429-25.143-10.286l-356.571-344c-4.571-4-130.286-118.857-130.286-256 
                                                0-167.429 102.286-267.429 273.143-267.429 100 0 193.714 78.857 238.857 123.429 45.143-44.571 138.857-123.429 
                                                238.857-123.429 170.857 0 273.143 100 273.143 267.429z" />
                                        </g>
                                    </svg>
                                </div>
                                <div class="my-4 bd-highlight">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20" data-tags="share-alt">
                                        <g fill="#444" transform="scale(0.01953125 0.01953125)">
                                            <path d="M260.096 622.592c-27.575 26.972-65.35 43.615-107.012 43.615-84.548 0-153.088-68.54-153.088-153.088 
                                            0-0.393 0.001-0.787 0.004-1.179l-0 0.060c0.054-84.79 68.802-153.505 153.6-153.505 
                                            41.373 0 78.926 16.358 106.544 42.959l-0.048-0.046 457.728-228.864c-0.768-5.77-1.206-12.441-1.206-19.214 0-84.831 
                                            68.769-153.6 153.6-153.6s153.6 68.769 153.6 153.6c0 84.831-68.769 153.6-153.6 153.6-41.285 
                                            0-78.765-16.288-106.366-42.788l0.052 0.050-457.728 228.864c0.739 5.688 1.161 12.267 1.161 
                                            18.944s-0.422 13.256-1.24 19.711l0.079-0.767 457.728 228.864c27.575-26.972 65.35-43.615 107.012-43.615 
                                            84.548 0 153.088 68.54 153.088 153.088 0 0.393-0.001 0.787-0.004 1.179l0-0.060c-0.727 84.277-69.215 
                                            152.316-153.595 152.316-84.831 0-153.6-68.769-153.6-153.6 0-6.223 0.37-12.36 1.089-18.389l-0.071 0.729-457.728-228.864z" />
                                        </g>
                                    </svg>
                                </div>
                            </div>
                            <p>
                                <h6 class="card-title">Браслет</h6>
                            </p>
                            <p>
                                5 533 р.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mt-3 mb-3">
                    <div class="card img-mask-1">
                        <img src="/weblab/images/catalog/silver/3.png" class="img-fluid card-img-top" alt="...">
                        <div class="card-body">
                            <div class="card-top-right">
                                <div class="card-percent">
                                    -25%
                                </div>
                                <div class="my-4 bd-highlight">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 28 28" data-code="61578" data-tags="heart-o">
                                        <g fill="#444" transform="scale(0.02734375 0.02734375)">
                                            <path d="M950.857 340.571c0-160.571-108.571-194.286-200-194.286-85.143 0-181.143 92-210.857 
                                                127.429-13.714 16.571-42.286 16.571-56 0-29.714-35.429-125.714-127.429-210.857-127.429-91.429 
                                                0-200 33.714-200 194.286 0 104.571 105.714 201.714 106.857 202.857l332 320 331.429-319.429c1.714-1.714 
                                                107.429-98.857 107.429-203.429zM1024 340.571c0 137.143-125.714 252-130.857 257.143l-356 342.857c-6.857 
                                                6.857-16 10.286-25.143 10.286s-18.286-3.429-25.143-10.286l-356.571-344c-4.571-4-130.286-118.857-130.286-256 
                                                0-167.429 102.286-267.429 273.143-267.429 100 0 193.714 78.857 238.857 123.429 45.143-44.571 138.857-123.429 
                                                238.857-123.429 170.857 0 273.143 100 273.143 267.429z" />
                                        </g>
                                    </svg>
                                </div>
                                <div class="my-4 bd-highlight">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20" data-tags="share-alt">
                                        <g fill="#444" transform="scale(0.01953125 0.01953125)">
                                            <path d="M260.096 622.592c-27.575 26.972-65.35 43.615-107.012 43.615-84.548 0-153.088-68.54-153.088-153.088 
                                            0-0.393 0.001-0.787 0.004-1.179l-0 0.060c0.054-84.79 68.802-153.505 153.6-153.505 
                                            41.373 0 78.926 16.358 106.544 42.959l-0.048-0.046 457.728-228.864c-0.768-5.77-1.206-12.441-1.206-19.214 0-84.831 
                                            68.769-153.6 153.6-153.6s153.6 68.769 153.6 153.6c0 84.831-68.769 153.6-153.6 153.6-41.285 
                                            0-78.765-16.288-106.366-42.788l0.052 0.050-457.728 228.864c0.739 5.688 1.161 12.267 1.161 
                                            18.944s-0.422 13.256-1.24 19.711l0.079-0.767 457.728 228.864c27.575-26.972 65.35-43.615 107.012-43.615 
                                            84.548 0 153.088 68.54 153.088 153.088 0 0.393-0.001 0.787-0.004 1.179l0-0.060c-0.727 84.277-69.215 
                                            152.316-153.595 152.316-84.831 0-153.6-68.769-153.6-153.6 0-6.223 0.37-12.36 1.089-18.389l-0.071 0.729-457.728-228.864z" />
                                        </g>
                                    </svg>
                                </div>
                            </div>
                            <p>
                                <h6 class="card-title">Часы</h6>
                            </p>
                            <p>
                                4 100 р.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mt-3 mb-3">
                    <div class="card img-mask-1">
                        <img src="/weblab/images/catalog/silver/4.png" class="img-fluid card-img-top" alt="...">
                        <div class="card-body">
                            <div class="card-top-right">
                                <div class="card-percent">
                                    -15%
                                </div>
                                <div class="my-4 bd-highlight">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 28 28" data-code="61578" data-tags="heart-o">
                                        <g fill="#444" transform="scale(0.02734375 0.02734375)">
                                            <path d="M950.857 340.571c0-160.571-108.571-194.286-200-194.286-85.143 0-181.143 92-210.857 
                                                127.429-13.714 16.571-42.286 16.571-56 0-29.714-35.429-125.714-127.429-210.857-127.429-91.429 
                                                0-200 33.714-200 194.286 0 104.571 105.714 201.714 106.857 202.857l332 320 331.429-319.429c1.714-1.714 
                                                107.429-98.857 107.429-203.429zM1024 340.571c0 137.143-125.714 252-130.857 257.143l-356 342.857c-6.857 
                                                6.857-16 10.286-25.143 10.286s-18.286-3.429-25.143-10.286l-356.571-344c-4.571-4-130.286-118.857-130.286-256 
                                                0-167.429 102.286-267.429 273.143-267.429 100 0 193.714 78.857 238.857 123.429 45.143-44.571 138.857-123.429 
                                                238.857-123.429 170.857 0 273.143 100 273.143 267.429z" />
                                        </g>
                                    </svg>
                                </div>
                                <div class="my-4 bd-highlight">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20" data-tags="share-alt">
                                        <g fill="#444" transform="scale(0.01953125 0.01953125)">
                                            <path d="M260.096 622.592c-27.575 26.972-65.35 43.615-107.012 43.615-84.548 0-153.088-68.54-153.088-153.088 
                                            0-0.393 0.001-0.787 0.004-1.179l-0 0.060c0.054-84.79 68.802-153.505 153.6-153.505 
                                            41.373 0 78.926 16.358 106.544 42.959l-0.048-0.046 457.728-228.864c-0.768-5.77-1.206-12.441-1.206-19.214 0-84.831 
                                            68.769-153.6 153.6-153.6s153.6 68.769 153.6 153.6c0 84.831-68.769 153.6-153.6 153.6-41.285 
                                            0-78.765-16.288-106.366-42.788l0.052 0.050-457.728 228.864c0.739 5.688 1.161 12.267 1.161 
                                            18.944s-0.422 13.256-1.24 19.711l0.079-0.767 457.728 228.864c27.575-26.972 65.35-43.615 107.012-43.615 
                                            84.548 0 153.088 68.54 153.088 153.088 0 0.393-0.001 0.787-0.004 1.179l0-0.060c-0.727 84.277-69.215 
                                            152.316-153.595 152.316-84.831 0-153.6-68.769-153.6-153.6 0-6.223 0.37-12.36 1.089-18.389l-0.071 0.729-457.728-228.864z" />
                                        </g>
                                    </svg>
                                </div>
                            </div>
                            <p>
                                <h6 class="card-title">Серьги</h6>
                            </p>
                            <p>
                                3 798 р.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php require_once '../footer.php'?>